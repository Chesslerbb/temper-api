<?php

declare(strict_types=1);

namespace App\Interfaces;

interface ChartService
{
    public function loadData(): array;
}
