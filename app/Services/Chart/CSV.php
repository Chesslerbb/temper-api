<?php

declare(strict_types=1);

namespace App\Services\Chart;

use App\Interfaces\ChartService;
use DateTime;
use Exception;

class CSV implements ChartService
{
    private const FILE_PATH = 'export.csv';

    private const FOPEN_MODE = 'r';

    private const CSV_SEPARATOR = ';';

    private const DATE_FIELD = 'created_at';

    private const STEP_FIELD = 'onboarding_perentage';

    private const STEPS = [
        0, 20, 40, 50, 70, 90, 99, 100
    ];

    private const STEP_LABELS = [
        0 => 'Create Account',
        20 => 'Activate Account',
        40 => 'Profile information',
        50 => 'Job interests',
        70 => 'Job Experience',
        90 => 'Freelancer',
        99 => 'Approval',
        100 => 'Approved',
    ];

    protected array $data = [];

    public function loadData(string $fileName = null): array
    {
        $filePath = storage_path($fileName ?: self::FILE_PATH);

        if (!file_exists($filePath)) {
            throw new Exception('File does not exist');
        }

        $resource = fopen($filePath, self::FOPEN_MODE);

        while (($line = fgetcsv($resource, null, self::CSV_SEPARATOR)) !== false) {
            $this->data[] = $line;
        }

        return $this
            ->setHeaders()
            ->groupByWeek()
            ->groupByStep()
            ->getPercentages()
            ->stepsToLabels()
            ->toSeries();
    }

    private function setHeaders(): static
    {
        $headers = array_shift($this->data);

        foreach ($this->data as $key => $line) {
            $this->data[$key] = array_combine($headers, $line);
        }

        return $this;
    }

    private function toSeries(): array
    {
        $series = [];

        foreach ($this->data as $date => $values) {
            $actualValues = [];

            foreach ($values as $step => $value) {
                $actualValues[] = [$step, $value];
            }

            $series[] = [
                'name' => $date,
                'data' => $actualValues,
            ];
        }

        return $series;
    }

    private function groupByWeek(): static
    {
        $groupedData = [];

        foreach ($this->data as $values) {
            $dateTime = DateTime::createFromFormat('Y-m-d', $values[self::DATE_FIELD]);

            $monday = $dateTime->modify('Monday this week')->format('Y-m-d');

            $groupedData[$monday][] = $values;
        }

        $this->data = $groupedData;

        return $this;
    }

    private function groupByStep(): static
    {
        $groupedData = [];

        foreach ($this->data as $date => $values) {
            $groupedData[$date] = [];

            foreach (self::STEPS as $step) {
                $groupedData[$date][$step] = 0;
            }

            foreach ($values as $user) {
                if (!key_exists($user[self::STEP_FIELD], $groupedData[$date])) {
                    $step = $this->determineStep($user[self::STEP_FIELD]);
                    $groupedData[$date][$step] += 1;

                    continue;
                }

                $groupedData[$date][$user[self::STEP_FIELD]] += 1;
            }
        }

        $this->data = $groupedData;

        return $this;
    }

    private function getPercentages(): static
    {
        foreach ($this->data as $date => $values) {
            $totalUsers = array_sum($values);

            $passed = $totalUsers;

            foreach ($values as $step => $userCount) {
                if ($step === self::STEPS[0]) {
                    $this->data[$date][$step] = 100;
                    continue;
                }

                if ($totalUsers === 0) {
                    $this->data[$date][$step] = 0;
                    continue;
                }

                $passed -= $userCount;

                $this->data[$date][$step] = ($passed / $totalUsers) * 100;
            }
        }

        return $this;
    }

    private function stepsToLabels(): static
    {
        foreach ($this->data as $date => $values) {
            foreach ($values as $key => $value) {
                $this->data[$date][self::STEP_LABELS[$key]] = $value;
                unset($this->data[$date][$key]);
            }
        }

        return $this;
    }

    private function determineStep(mixed $value): int
    {
        // @TODO Made an assumption what to do with these.
        // Naturally I would discuss these edge-cases with PO or team-members.
        if (empty($value) || !is_numeric($value)) {
            return self::STEPS[0];
        }

        foreach (self::STEPS as $index => $step) {
            if ($value >= $step && $value < self::STEPS[$index + 1]) {
                return $step;
            }
        }
    }
}
