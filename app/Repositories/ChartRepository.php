<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Interfaces\ChartService;
use App\Services\Chart\CSV;
use Exception;

class ChartRepository
{
    private ?ChartService $chartService = null;

    public function show(): array
    {
        return $this->from(CSV::class)->get();
    }

    protected function get(): array
    {
        if (!$this->chartService) {
            throw new Exception('Must set ChartService first');
        }

        return $this->chartService->loadData();
    }

    protected function from(string $class): static
    {
        if (!class_exists($class)) {
            throw new Exception('Class does not exist');
        }

        $service = new $class();

        if (!$service instanceof ChartService) {
            throw new Exception('Class is does not implement ChartService interface');
        }

        $this->chartService = $service;

        return $this;
    }
}
