<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\ChartRepository;
use Illuminate\Http\JsonResponse;

class ChartController extends Controller
{
    private ChartRepository $chartRepository;

    public function __construct(ChartRepository $chartRepository)
    {
        $this->chartRepository = $chartRepository;
    }

    public function index(): JsonResponse
    {
        return response()->json(
            $this->chartRepository->show()
        );
    }
}
