## Installation

1. Clone this repository
2. `cd` into folder of cloned repository and run `php artisan serve`
3. Clone front-end repository (https://bitbucket.org/Chesslerbb/temper-web/)
4. `cd` into folder of cloned repository and run `npm install && npm run serve`
5. Browse to `http://localhost:8080/` to view the Chart.


## Preview of result
![Chart Preview](ChartPreview.png)
