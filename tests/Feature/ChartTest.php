<?php

namespace Tests\Feature;

use Tests\TestCase;

class ChartTest extends TestCase
{
    /** @test */
    public function it_will_return_chartable_data_from_a_csv(): void
    {
        $result = $this->getJson(route('charts.index'));

        $result->assertOk();

        $result->assertJsonStructure([
            ['name', 'data'],
        ]);
    }
}
