<?php

use App\Http\Controllers\ChartController;
use Illuminate\Support\Facades\Route;

Route::apiResource('charts', ChartController::class)->only(['index']);
